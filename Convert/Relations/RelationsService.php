<?php
/**
 * Created by PhpStorm.
 * User: Dima
 * Date: 02.11.2019
 * Time: 16:09
 */

namespace App\Import\Convert\Relations;


use Illuminate\Support\Facades\DB;

class RelationsService
{

    public function getRelationValue($field, $item)
    {
        switch($field['relation']['type'])
        {
            case 'one':
                return $this->getRelationValueOne($field, $item);
                break;
            case 'two':
                return $this->getRelationValueTwo($field, $item);
                break;
        }
        return null;
    }

    public function getRelationValueOne($field, $item)
    {
        $relation_object = [];

        $relation = DB::table($field['relation']['table'])->where($field['relation']['foreign_field'], $item['value'])->first();
        if($relation !== null) {
            if(isset($field['key'])) {
                $relation_object['key'] = $field['key'];
            }
            $relation_object['value'] = $relation->{$field['relation']['foreign_key']};
            $relation_object['is_exists'] = true;
        }
        else
        {
            if(isset($field['key'])) {
                $relation_object['key'] = $field['key'];
            }
            $relation_object['value'] = [
                'table' => $field['relation']['table'],
                'field' => $field['relation']['foreign_field'],
                'value' => $item['value'],
                'return' => $field['relation']['foreign_key'],
            ];
            $relation_object['is_exists'] = false;
        }

        $relation_object['is_relation'] = true;
        $relation_object['relation'] = 'one';

        if($item['value'] === null) return false;

        return $relation_object;
    }

    public function getRelationValueTwo($field, $item)
    {
        $relation_object = [];

        $key_relation = DB::table($field['relation']['key_table'])->where($field['relation']['key_table_foreign_field'], $item['key'])->first();

        if($key_relation !== null)
        {
            $value_relation = DB::table($field['relation']['value_table'])->where($field['relation']['value_table_foreign_field'], $item['value'])->where($field['relation']['value_table_relation_field'], $key_relation->{$field['relation']['key_table_foreign_key']})->first();

            if($value_relation !== null) {
                $relation_object['key'] = $field['key'];
                $relation_object['value'] = $value_relation->{$field['relation']['value_table_foreign_key']};
                $relation_object['is_exists'] = true;
            }
            else
            {
                if(isset($field['key'])) {
                    $relation_object['key'] = $field['key'];
                }
                $relation_object['value'] = [
                    'key_table_value' => $key_relation->{$field['relation']['key_table_foreign_key']},
                    'key_table_exists' => true,
                    'value_table' => $field['relation']['value_table'],
                    'value_table_field' => $field['relation']['value_table_foreign_field'],
                    'value_table_value' => $item['value'],
                    'value_table_return' => $field['relation']['value_table_foreign_key'],
                    'value_table_relation_field' => $field['relation']['value_table_relation_field'],
                ];
                $relation_object['is_exists'] = false;
            }
        }
        else
        {
            if(isset($field['key'])) {
                $relation_object['key'] = $field['key'];
            }
            $relation_object['value'] = [
                'key_table' => $field['relation']['key_table'],
                'key_table_field' => $field['relation']['key_table_foreign_field'],
                'key_table_value' => $item['key'],
                'key_table_return' => $field['relation']['key_table_foreign_key'],
                'key_table_exists' => false,
                'value_table' => $field['relation']['value_table'],
                'value_table_field' => $field['relation']['value_table_foreign_field'],
                'value_table_value' => $item['value'],
                'value_table_return' => $field['relation']['value_table_foreign_key'],
                'value_table_relation_field' => $field['relation']['value_table_relation_field'],
            ];
            $relation_object['is_exists'] = false;
        }

        $relation_object['is_relation'] = true;

        $relation_object['relation'] = 'two';

        if($item['value'] === null) return false;

        return $relation_object;
    }

}