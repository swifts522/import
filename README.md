Проект собран для фреймворка Laravel. Конфигурационный файл должен находится в config/import.php.
Пример структуры конфигурации:

```
return [

    'margin' => [
        'top' => 3,
        'left' => 1,
    ],

    'schema' => 'vertical', // vertical, horizontal

    'separator' => [
        'Поле карточки товара' => 'Содержание'
    ],

    'position' => [
        'key' => 1,
        'value' => 3,
    ],

    'id' => [
        'field' => 'code'
    ],

    'fields' => [
        'title' => [
            'key' => 'title',
            'position' => 1,
            'type' => 'string',
            'required' => true,
        ],
        'price' => [
            'key' => 'price',
            'position' => 2,
            'type' => 'integer',
            'required' => true,
            'default' => 1000,
        ],
        'code' => [
            'key' => 'code',
            'position' => 12,
            'type' => 'integer',
            'required' => true,
        ],
        'height' => [
            'key' => 'height',
            'position' => 24,
            'type' => 'integer',
            'required' => false,
            'default' => 10,
        ],
        'category_id' => [
            'key' => 'category_id',
            'position' => 3,
            'type' => 'integer',
            'relation' => [
                'type' => 'one',
                'table' => 'categories',
                'foreign_key' => 'id',
                'foreign_field' => 'title'
            ],
            'required' => true,
        ],
        'feature_values' => [
            'key' => 'feature_values',
            'type' => 'composite',
            'keys' => [
                'Возраст' => [
                    'position' => 4,
                    'type' => 'string',
                    'required' => false,
                ],
                'Материал верха' => [
                    'position' => 5,
                    'type' => 'string',
                    'required' => false,
                ],
                'Материал (для мебели)' => [
                    'position' => 6,
                    'type' => 'string',
                    'required' => false,
                ],
                'Сезон' => [
                    'position' => 7,
                    'type' => 'string',
                    'required' => false,
                ],
                'Основа конструкции' => [
                    'position' => 20,
                    'type' => 'string',
                    'required' => false,
                ],
                'Чехол' => [
                    'position' => 21,
                    'type' => 'string',
                    'required' => false,
                ],
                'Обивка' => [
                    'position' => 22,
                    'type' => 'string',
                    'required' => false,
                ],
                'Назначения' => [
                    'position' => 24,
                    'type' => 'string',
                    'required' => false,
                ],
                'Ситуация применения (особые)' => [
                    'position' => 29,
                    'type' => 'string',
                    'required' => false,
                ],
                'Локация применения' => [
                    'position' => 30,
                    'type' => 'string',
                    'required' => false,
                ],
                'Оказываемые эффекты' => [
                    'position' => 31,
                    'type' => 'string',
                    'required' => false,
                ],
                'Максимальная нагрузка на спальное место (кг)' => [
                    'position' => 32,
                    'type' => 'string',
                    'required' => false,
                ],
                'Жесткость' => [
                    'position' => 33,
                    'type' => 'string',
                    'required' => false,
                ],
                'Противопоказания к применению' => [
                    'position' => 34,
                    'type' => 'string',
                    'required' => false,
                ],
                'Комплектность' => [
                    'position' => 36,
                    'type' => 'string',
                    'required' => false,
                ],
            ],
            'relation' => [
                'type' => 'two',
                'key_table' => 'features',
                'key_table_foreign_key' => 'id',
                'key_table_foreign_field' => 'title',
                'value_table' => 'feature_values',
                'value_table_foreign_key' => 'id',
                'value_table_foreign_field' => 'title',
                'value_table_relation_field' => 'feature_id',
            ],
            'json_struct' => '{"feature_value_id": $value$}',
            'required' => false,
        ],
        'option_values' => [
            'key' => 'option_values',
            'type' => 'composite',
            'keys' => [
                'Цвет' => [
                    'position' => 13,
                    'type' => 'string',
                    'required' => false,
                ],
                'Размеры' => [
                    'position' => 23,
                    'type' => 'many',
                    'separator' => ',',
                    'required' => false,
                ],
            ],
            'relation' => [
                'type' => 'two',
                'key_table' => 'options',
                'key_table_foreign_key' => 'id',
                'key_table_foreign_field' => 'title',
                'value_table' => 'option_values',
                'value_table_foreign_key' => 'id',
                'value_table_foreign_field' => 'title',
                'value_table_relation_field' => 'option_id',
            ],
            'json_struct' => '{"price_effect": "+", "price_offset": 0, "option_value_id": $value$}',
            'required' => false,
        ],
        'composition' => [
            'key' => 'composition',
            'type' => 'composite',
            'keys' => [
                'Состав материалов (общий)' => [
                    'position' => 36,
                    'type' => 'many',
                    'separator' => ',',
                    'required' => false,
                ],
            ],
            'json_struct' => '{"value": "$value$"}',
            'required' => false,
        ],
        'advantages' => [
            'key' => 'advantages',
            'type' => 'composite',
            'keys' => [
                'Преимущества' => [
                    'position' => 26,
                    'type' => 'many',
                    'separator' => ',',
                    'required' => false,
                ],
            ],
            'json_struct' => '{"value": "$value$"}',
            'required' => false,
        ],
        'images' => [
            'key' => 'images',
            'type' =>  'composite',
            'keys' => [
                'Детальная картинка' => [
                    'position' => 9,
                    'type' => 'google_image',
                ],
                'Дополнительные изображения' => [
                    'position' => 10,
                    'type' => 'many',
                    'separator' => ',',
                    'required' => false,
                ],
            ],
            'json_struct' => '{"value": "$value$"}',
            'required' => false,
        ],
    ]

];
```
