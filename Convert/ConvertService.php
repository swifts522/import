<?php
/**
 * Created by PhpStorm.
 * User: Dima
 * Date: 01.11.2019
 * Time: 18:22
 */

namespace App\Import\Convert;


use App\Import\Convert\Composits\CompositsService;
use App\Import\Convert\Relations\RelationsService;
use App\Import\Convert\Types\TypesSerivce;
use App\Import\ImportService;

class ConvertService
{
    private $config;
    private $relation;
    private $types;
    private $composit;

    public function __construct($config)
    {
        $this->config = $config;
        $this->relation = new RelationsService();
        $this->types = new TypesSerivce();
        $this->composit = new  CompositsService();
    }

    public function getConvertItem($item)
    {
        $fields = [];
        foreach($this->config as $key=>$field)
        {
            $func = 'get'.ucfirst($field['type']);
            if(isset($field['position'])) {
                $func = 'get' . ucfirst($field['type']);
                $fields[$key] = $this->types->$func($field, $item[$field['position']]);
                if(isset($fields[$key]['required']) && $fields[$key]['required'] == true && $fields[$key] == false) return false;
                if($fields[$key] == false)
                {
                    unset($fields[$key]);
                }
            }
            else {
                $func = 'get' . ucfirst($field['type']);
                $fields[$key] = $this->types->$func($field, $item);
                if(isset($fields[$key]['required']) && $fields[$key]['required'] == true && $fields[$key] == false) return false;
                if($fields[$key] == false)
                {
                    unset($fields[$key]);
                }
            }
        }

        return $fields;
    }
}