<?php
/**
 * Created by PhpStorm.
 * User: Dima
 * Date: 04.11.2019
 * Time: 12:32
 */

namespace App\Import\Load;


use App\Import\Load\Types\TypesService;
use Illuminate\Support\Facades\DB;

class LoadService
{
    private $types;

    public function __construct()
    {
        $this->types = new TypesService();
    }

    public function convertItem($item, $model)
    {
        foreach($item as $key=>$attribute)
        {
            if(isset($attribute['keys']))
            {
                $model[$key] = $this->getComposit($attribute);
            }
            else
            {
                $model[$key] = $this->getField($attribute);
            }
            if($model[$key] === null) {
                unset($model[$key]);
            }
        }

        return $model;
    }

    public function getComposit($item)
    {
        $items = [];

        foreach($item['keys'] as $key)
        {
            $val = $this->getField($key);

            if($val === null) continue;

            if(is_array($val))
            {
                foreach($val as $v)
                {
                    if(isset($key['json_struct'])) {
                        $items[] = str_replace('$value$', $v, $key['json_struct']);
                    }
                    else if($item['json_struct'])
                    {
                        $items[] = str_replace('$value$', $v, $item['json_struct']);
                    }
                    else
                    {
                        $items[] = $v;
                    }
                }
            }
            else
            {
                if (isset($key['json_struct'])) {
                    $items[] = str_replace('$value$', $val, $key['json_struct']);
                } else if ($item['json_struct']) {
                    $items[] = str_replace('$value$', $val, $item['json_struct']);
                } else {
                    $items[] = $val;
                }
            }
        }

        if(isset($item['json_struct']))
        {
            $items = implode(',', $items);
            $items = '['.$items.']';
        }

        $items = json_decode($items);

        return $items;
    }

    public function getField($item)
    {
        if(isset($item['is_many']) && $item['is_many'])
        {
            $items = [];
            foreach($item['value'] as $key=>$val)
            {
                $items[$key] = $this->getField($val);
                if($items[$key] === null)
                {
                    unset($items[$key]);
                }
            }

            return $items;
        }
        else
        {
            if(isset($item['is_relation']) && $item['is_relation'])
            {
                return $this->getRelationValue($item);
            }
            else
            {
                if(isset($item['value'])) {
                    return $item['value'];
                }
            }
        }

        return null;
    }

    public function getRelationValue($item)
    {
        if($item['is_exists'] == false)
        {
            if($item['relation'] == 'two')
            {
                if($item['value']['key_table_value'] == null) return null;
                if (isset($item['value']['key_table'])) {
                    $key_table = DB::table($item['value']['key_table'])->where($item['value']['key_table_field'], $item['value']['key_table_value'])->first();

                    if ($key_table !== null) {
                        $item['value']['key_table_exists'] = true;
                        $item['value']['key_table_value'] = $key_table->{$item['value']['key_table_return']};
                    }
                }

                if (isset($item['value']['key_table_exists']) && $item['value']['key_table_exists'] == true) {
                    $value_table = DB::table($item['value']['value_table'])->where($item['value']['value_table_field'], $item['value']['value_table_value'])->where($item['value']['value_table_relation_field'], $item['value']['key_table_value'])->first();

                    if ($value_table === null) {
                        $value_table = DB::table($item['value']['value_table'])->insertGetId([$item['value']['value_table_field'] => $item['value']['value_table_value'], $item['value']['value_table_relation_field'] => $item['value']['key_table_value']]);
                    } else {
                        $value_table = $value_table->{$item['value']['value_table_return']};
                    }
                } else {
                    $key_table = DB::table($item['value']['key_table'])->insertGetId([$item['value']['key_table_field'] => $item['value']['key_table_value']]);

                    if ($item['value']['value_table_value'] == null) return null;

                    $value_table = DB::table($item['value']['value_table'])->insertGetId([$item['value']['value_table_field'] => $item['value']['value_table_value'], $item['value']['value_table_relation_field'] => $key_table]);
                }

                $item['value'] = $value_table;
            }
            else
            {
                if(!isset($item['is_exists']))
                {
                    if($item['value']['value'] == null) return null;

                    $key = DB::table($item['value']['table'])->where($item['value']['field'], $item['value']['value'])->first();

                    $item['value'] = $key->{$item['value']['return']};
                }

                if($item['value'] == null) return null;
            }

        }

        return $item['value'];
    }
}