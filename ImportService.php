<?php
/**
 * Created by PhpStorm.
 * User: Dima
 * Date: 31.10.2019
 * Time: 17:08
 */

namespace App\Import;

use App\Import\Convert\ConvertService;
use Illuminate\Support\Facades\Config;

use PHPExcel;
use PHPExcel_IOFactory;


class ImportService
{
    protected $document;

    protected $margin;

    private $worksheet;
    private $highestRow;
    private $products;
    private $field;

    private $convert;

    public function __construct($path)
    {
        $this->document = PHPExcel_IOFactory::load($path);
        $this->worksheet = $this->document->getActiveSheet();
        $this->highestRow = $this->worksheet->getHighestRow();
        $this->margin = $this->getConfig('margin');
        $this->field = $this->getConfig('fields');
        $this->convert = new ConvertService($this->field);
    }

    public function getRawData()
    {
        $products = $this->separatorProducts();

        return $products;
    }

    public function getData()
    {
        $this->setRawData();

        $convert = $this->convert;

        foreach($this->products as $key=>$data)
        {
            $this->products[$key] = $convert->getConvertItem($data);
            if($this->products[$key] == false) {
                unset($this->products[$key]);
            }
        }

        return $this->products;
    }

    protected function setRawData()
    {
        $this->products = $this->getRawData();
    }

    protected function getConfig($config)
    {
        $conf = Config::get('import.'.$config);
        switch($config)
        {
            case 'separator': {
                return [key($conf), reset($conf)];
            }
        }
        return $conf;
    }

    protected function getMargin($item, $type = 'left')
    {
        return $this->margin[$type]+($item-1);
    }

    private function separatorProducts()
    {
        $worksheet = $this->worksheet;
        $separator = $this->getConfig('separator');
        $position = $this->getConfig('position');
        $products = [];

        $i = 0;
        $j = 0;
        for ($row = $this->getMargin(1, 'top'); $row <= $this->highestRow; ++$row) {

            $rows = [];

            $rows[0] = $worksheet->getCellByColumnAndRow($this->getMargin($position['key']), $row)->getValue();
            $rows[1] = $worksheet->getCellByColumnAndRow($this->getMargin($position['value']), $row)->getValue();

            if($rows[0] == $separator[0] && $rows[1] == $separator[1])
            {
                $i++;
                $j = 0;
            }
            else {
                $products[$i][$j]['key'] = $worksheet->getCellByColumnAndRow($this->getMargin($position['key']), $row)->getValue();
                $products[$i][$j]['value'] = $worksheet->getCellByColumnAndRow($this->getMargin($position['value']), $row)->getValue();

                $j++;
            }
        }

        return $products;
    }
}