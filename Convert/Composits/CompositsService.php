<?php
/**
 * Created by PhpStorm.
 * User: Dima
 * Date: 02.11.2019
 * Time: 16:09
 */

namespace App\Import\Convert\Composits;


use App\Import\Convert\Relations\RelationsService;
use App\Import\Convert\Types\TypesSerivce;

class CompositsService
{
    /*private $relation;
    private $types;

    public function __construct()
    {
        $this->relation = new RelationsService();
        $this->types = new TypesSerivce();
    }*/

    public function getCompositsField($field, $item)
    {
        $object = [];
        $object['key'] = $field['key'];
        $types = new TypesSerivce();
        foreach($field['keys'] as $k=>$key)
        {
            if(!isset($item[$key['position']]['value']) || $item[$key['position']]['value'] === null) continue;

            if(isset($field['relation'])) {
                $key['relation'] = $field['relation'];
            }

            $key['key'] = $k;

            $func = 'get' . ucfirst($key['type']);
            $object['keys'][$k] = $types->$func($key, $item[$key['position']]);

            if(isset($field['json_struct'])) {
                $object['keys'][$k]['json_struct'] = $field['json_struct'];
            }

            if(!isset($object['keys'][$k]['value']) || $object['keys'][$k]['value'] == null) {
                unset($object['keys'][$k]);
            }
        }

        if(isset($field['json_struct'])) {
            $object['json_struct'] = $field['json_struct'];
        }

        return $object;
    }
}