<?php
/**
 * Created by PhpStorm.
 * User: Dima
 * Date: 01.11.2019
 * Time: 17:59
 */

namespace App\Import\Convert\Types;


use App\Import\Convert\Composits\CompositsService;
use App\Import\Convert\Relations\RelationsService;
use App\Import\ImportService;

class TypesSerivce
{
    private $relation;
    private $composite;

    public function __construct()
    {
        $this->relation = new RelationsService();
        $this->composite = new CompositsService();
    }

    public function getString($field, $item)
    {
        $object = [];

        if(isset($field['relation'])) {
            $object = $this->relation->getRelationValue($field, $item);
        }
        else
        {
            if(isset($field['key'])) {
                $object['key'] = $field['key'];
            }
            $object['value'] = $item['value'];
        }

        if($object['value'] === null)
        {
            if(isset($field['default']))
            {
                $object['value'] = $field['default'];
            }
        }

        if($object['value'] === null)
        {
            return false;
        }

        return $object;
    }

    public function getInteger($field, $item)
    {
        $object = [];

        if(isset($field['relation'])) {
            $object = $this->relation->getRelationValue($field, $item);
        }
        else
        {
            if(isset($field['key'])) {
                $object['key'] = $field['key'];
            }
            $object['value'] = $item['value'];
        }

        if($object['value'] === null)
        {
            if(isset($field['default']))
            {
                $object['value'] = $field['default'];
            }
        }

        if($object['value'] == null) return false;

        return $object;
    }

    public function getMany($field, $item, $type_item = 'string')
    {
        /*if(isset($field['relation'])) {
            $object = $this->relation->getRelationValue($field, $item);
        }*/

        if(isset($field['key'])) {
            $object['key'] = $field['key'];
        }
        $values = explode($field['separator'], $item['value']);

        if($values == null) return false;

        foreach($values as $val)
        {
            if(isset($field['relation'])) {
                $itm['key'] = $field['key'];
                $itm['value'] = $val;
                $object['value'][] = $this->relation->getRelationValue($field, $itm);
            }
            else {
                if(isset($field['key'])) {
                    $object['key'] = $field['key'];
                }
                $object['value'][]['value'] = $val;
            }
        }
        $object['is_many'] = true;

        return $object;
    }

    public function getComposite($field, $item)
    {
        $object = $this->composite->getCompositsField($field, $item);

        return $object;
    }

    public function getGoogle_image($field, $item)
    {
        if(isset($field['key'])) {
            $object['key'] = $field['key'];
        }
        $object['value'] = $item['value'];

        if($object['value'] === null)
        {
            if(isset($field['default']))
            {
                $object['value'] = $field['default'];
            }
        }

        if($object['value'] === null)
        {
            return false;
        }

        return $object;
    }
}